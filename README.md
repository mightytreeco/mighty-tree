Mighty Tree is an award-winning Marijuana dispensary serving Medical and Recreational patients only the very best in premium Colorado cannabis products. Experience the Mighty difference at our North and South Denver locations today.

Address: 4755 Lipan St, Denver, CO 80211, USA

Phone: 303-600-8961

Website: http://www.mightytreeco.com/
